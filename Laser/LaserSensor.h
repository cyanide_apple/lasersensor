/****************************
*LaserSensor.h				*
*****************************
*author : Zeki Aydin        *
*IDE : Visual Studio 2017   *
*Last Update: 21/12/2018    *
*****************************/

#ifndef LaserSensor_h
#define LaserSensor_h
#include<iostream>
#include"PioneerRobotAPI.h"
#include"RangeSensor.h"
using namespace std;

/**
*\brief LaserSensor class operates the laser sensors.
*/
class LaserSensor: public RangeSensor
{
//private:
//	/**
//	*\brief Storing ranges in an array.
//	*/
//	float ranges[181];
//	/**
//	*\brief Points adress of robot.
//	*/
//	PioneerRobotAPI *robotAPI;
public:
	/**
	*\brief Constructor.
	*/
	LaserSensor(PioneerRobotAPI *robotAPI);
	/**
	*\brief Destructor.
	*/
	~LaserSensor();
	/**
	*\brief It returns the range of given index.
	*\param index                         : given index.
	*\return the range of given index.
	*/
	float getRange(int index);
	/**
	*\brief It fills the "ranges" array up by the laser sensor ranges.
	*\param index                         : given array.
	*/
	void updateSensor(float ranges[]);
	/**
	*\brief It returns the maximum value of ranges array.
	*\param index                         : given index.
	*\return Maximum value.
	*/
	float getMax(int& index);
	/**
	*\brief It returns the minimum value of ranges array.
	*\param index                         : given index.
	*\return Maximum value.
	*/
	float getMin(int&index);
	/**
	*\brief It returns the range of given index.
	*\param index                         : given index.
	*\return the range of given index.
	*/
	float operator[](int i);
	/**
	*\brief It returns the angle of given index.
	*\param index                         : given index.
	*\return the angle of given index.
	*/
	float getAngle(int index);
	/**
	*\brief It returns the closest range between given start and end angles.
	*\param startAngle                         : Starting angle.
	*\param endAngle                           : Ending angle.
	*\param angle							   : Returning angle of closest.
	*\return the closest range.
	*/
	float getClosestRange(float startAngle, float endAngle, float& angle);
};

#endif // !LaserSensor_h