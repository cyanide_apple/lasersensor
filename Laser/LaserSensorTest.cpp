﻿#include<iostream>
#include"PioneerRobotAPI.h"
#include"LaserSensor.h"
using namespace std;

void test_laser_sensor()
{

	PioneerRobotAPI *robot = new PioneerRobotAPI();
	if (!robot->connect()) {
		cout << "Could not connect..." << endl;
		return;
	}
	while (1)
	{
		robot->moveRobot(100);
		LaserSensor lasersensor(robot);
		cout << endl << "Ranges for all lasers" << endl;
		for (int i = 0; i < 181; i++)
		{ 
			cout << i << ". sensor: " << lasersensor.getRange(i) << endl;

		}
		int max = 0;
		int min = 0;
		cout << "Maximum range is " << lasersensor.getMax(max) << " at index of " << max << endl;
		cout << "Minimum range is " << lasersensor.getMin(min) << " at index of " << min << endl;

		float ang = 0;
		float startAngle = 0;
		float endAngle = 10;
		cout << "Closest range between index " << startAngle << " and index " << endAngle << " is "  << lasersensor.getClosestRange(startAngle, endAngle, ang) << " at angle of " << ang <<endl;
		
		system("pause");
	}
}
int main()
{
	test_laser_sensor();
	return 0;
}