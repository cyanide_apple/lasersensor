/****************************
*LaserSensor.h				*
*****************************
*author : Zeki Aydin        *
*IDE : Visual Studio 2017   *
*Last Update: 21/12/2018    *
*****************************/

#include "LaserSensor.h"

LaserSensor::LaserSensor(PioneerRobotAPI *robotAPI)
{
	this->robotAPI = robotAPI;
	this->robotAPI->getLaserRange(ranges);
}
LaserSensor::~LaserSensor()
{

}
float LaserSensor::getRange(int index)
{
	return ranges[index];
}
void LaserSensor::updateSensor(float ranges[])
{
	for (int i = 0; i < 181; i++)
	{
		ranges[i] = this->ranges[i];
	}
}
float LaserSensor::getMax(int& index)
{
	float value = ranges[0];
	for (int i = 0; i < 181; i++)
	{
		if (ranges[i] > value)
		{
			value = ranges[i];
			index = i;
		}
	}
	return value;
}
float LaserSensor::getMin(int& index)
{
	float value = ranges[0];
	for (int i = 0; i < 181; i++)
	{
		if (ranges[i] < value)
		{
			value = ranges[i];
			index = i;
		}
	}
	return value;
}
float LaserSensor::operator[](int i)
{
	return ranges[i];
}
float LaserSensor::getAngle(int index)
{
	if (index >= 0 && index < 181)
	{
		return index;
	}
	return -1;
}
float LaserSensor::getClosestRange(float startAngle, float endAngle, float& angle)
{
	float closest = ranges[(int)startAngle];
	
	for (int i = startAngle;i < endAngle;i++)
	{
		if (ranges[i] < closest)
		{
			closest = ranges[i];
			angle = i;
		}
	}
	return closest;
}