/****************************
*RangeSensor.h				*
*****************************
*author : Zeki Aydin        *
*IDE : Visual Studio 2017   *
*Last Update: 27/12/2018    *
*****************************/

#ifndef RangeSensor_h
#define RangeSensor_h
#include<iostream>
#include"PioneerRobotAPI.h"
using namespace std;

/**
*\brief LaserSensor class operates the laser sensors.
*/
class RangeSensor
{
protected:
	
	/**
	*\brief Points adress of robot.
	*/
	PioneerRobotAPI *robotAPI;
	/**
	*\brief Storing ranges in an array.
	*/
	float ranges[181];
public:
	/**
	*\brief Default Constructor.
	*/
	RangeSensor() {};
	/**
	*\brief Constructor.
	*/
	RangeSensor(PioneerRobotAPI *robotAPI)
		{
		this->robotAPI = robotAPI;
		};
	/**
	*\brief Destructor.
	*/
	virtual ~RangeSensor() {};
	/**
	*\brief It returns the range of given index.
	*\param index                         : given index.
	*\return the range of given index.
	*/
	virtual float getRange(int index) = 0;
	/**
	*\brief It fills the "ranges" array up by the laser sensor ranges.
	*\param index                         : given array.
	*/
	virtual void updateSensor(float ranges[]) = 0;
	/**
	*\brief It returns the maximum value of ranges array.
	*\param index                         : given index.
	*\return Maximum value.
	*/
	virtual float getMax(int& index) = 0;
	/**
	*\brief It returns the minimum value of ranges array.
	*\param index                         : given index.
	*\return Maximum value.
	*/
	virtual float getMin(int& index) = 0;
	/**
	*\brief It returns the range of given index.
	*\param index                         : given index.
	*\return the range of given index.
	*/
	virtual float operator[](int i) = 0;
	/**
	*\brief It returns the angle of given index.
	*\param index                         : given index.
	*\return the angle of given index.
	*/
	virtual float getAngle(int index) = 0;
	/**
	*\brief It returns the closest range between given start and end angles.
	*\param startAngle                         : Starting angle.
	*\param endAngle                           : Ending angle.
	*\param angle							   : Returning angle of closest.
	*\return the closest range.
	*/
	virtual float getClosestRange(float startAngle, float endAngle, float& angle) = 0;
};

#endif // !RangeSensor_h